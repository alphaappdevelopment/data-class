package app.alphaapp.dataclass;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class DataClass extends Activity {

	//Data Class attributes
	static char BACKSLASH = '\\';
	public static int myUid, resourceId;
	public static String locationOfPicture,pictureDescription;
	
	public PictureDataClass bob;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_class);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.data_class, menu);
		return true;
	}

	public static void main(String args[])
	{
		
		myUid = 10;
		resourceId = 3;
		locationOfPicture = "C:"+BACKSLASH+"AllThepicturesAreHere"+BACKSLASH; 
		pictureDescription = "A nice playground";
		
		PictureDataClass bob2 = new PictureDataClass(myUid,resourceId,locationOfPicture,pictureDescription);
		
		
	}

}
